package triton_docker_machine

import (
	"fmt"
	"io/ioutil"
	"gitlab.com/gitlab-org/gitlab-ci-multi-runner/common"
	"os"
	"time"
	"os/exec"
	// Force to load docker executor
	_ "gitlab.com/gitlab-org/gitlab-ci-multi-runner/executors/docker"
	"github.com/joyent/gocommon/client"
	"github.com/joyent/gosdc/cloudapi"
    "github.com/joyent/gosign/auth"
    docker_helpers "gitlab.com/gitlab-org/gitlab-ci-multi-runner/helpers/docker"
    "sync"
)

type Machine struct {
	Name 		string
	State		string
	Ready 		bool
	WaitGroup	sync.WaitGroup
	Error 		error
	DockerCred 	docker_helpers.DockerCredentials
	Machine 	*cloudapi.Machine
	Builds 		int
	RemoveTimer *time.Timer
}

type machinePool struct {
	Config     *common.RunnerConfig
	Triton 		*cloudapi.Client
	MachinesLock *sync.Mutex
	Machines 	map[string]*Machine
}

func (mp *machinePool) machineNameForBuild(build *common.Build) string {
	buildVars := build.GetAllVariables()
	pipelineId := ""
	runnerId := ""
	for _, variable := range buildVars {
		if (variable.Key == "CI_PIPELINE_ID"){
			pipelineId = variable.Value
		}
		if (variable.Key == "CI_RUNNER_ID"){
			runnerId = variable.Value
		}
	}
	return fmt.Sprintf("gitlabci-project%d-pipeline%s-runner%s", build.ProjectID, pipelineId, runnerId)
}

func (mp *machinePool) init() {
	mp.MachinesLock = &sync.Mutex{}
	mp.Machines = make(map[string]*Machine)
} 

func (mp *machinePool) connectTriton() (err error) {
	if (mp.Triton != nil) {
		return nil
	}
	keyPath := fmt.Sprintf("%s/key.pem", mp.Config.TritonDockerMachine.CertPath)
	keyData, err := ioutil.ReadFile(keyPath)
	if err != nil {
		return err
	}
	userAuth, err := auth.NewAuth(mp.Config.TritonDockerMachine.TritonUser, string(keyData), "rsa-sha256")
	if err != nil {
		return err
	}
	creds := &auth.Credentials{
        UserAuthentication: userAuth,
        SdcKeyId:           mp.Config.TritonDockerMachine.TritonKeyId,
        SdcEndpoint:        auth.Endpoint{URL: mp.Config.TritonDockerMachine.TritonHost},
    }
    mp.Triton = cloudapi.New(client.NewClient(
        creds.SdcEndpoint.URL,
        cloudapi.DefaultAPIVersion,
        creds,
        nil,
    ))
    _, err = mp.Triton.ListKeys()
    if err != nil {
		return err
	}
    return
}

func (mp *machinePool) createTritonInstance(machineName string) *cloudapi.Machine {
	machine, err := mp.Triton.CreateMachine(cloudapi.CreateMachineOpts{
		Name: machineName,
		Package: mp.Config.TritonDockerMachine.DefaultPackage,
		Image: mp.Config.TritonDockerMachine.MachineImage,
		Networks: []string{mp.Config.TritonDockerMachine.Network},
		FirewallEnabled: false,
	})
	if (err != nil) {
		fmt.Println("Machine Create Error", err)
		//the request has failed - wait a minute polling getmachines to see if it shows up
		hasMachine := false
		checkCount := 0
		machineFilter := cloudapi.NewFilter()
		machineFilter.Set("name", machineName)
		for ((hasMachine == false) && (checkCount < 12)) {
			fmt.Println("Listing Machines")
			machines, listErr := mp.Triton.ListMachines(machineFilter)
			fmt.Println(machines, listErr)
			for _, m := range machines {
				fmt.Println("Checking machine with name", m.Name, "compared with", machineName)
				if (m.Name == machineName) {
					machine = &m
					hasMachine = true
					fmt.Println("Found machine that was meant to be created")
				}
			}
			checkCount += 1
			time.Sleep(5 * time.Second)
		}
	}
	if (machine == nil) {
		return mp.createTritonInstance(machineName)
	}
	return machine
} 

func (mp *machinePool) buildHost(machineName string) {
	machineObj := mp.Machines[machineName]
	machine := mp.createTritonInstance(machineName)
	//if success, we need to wait until its running and then fetch the IP
	machineRunning := false
	for (machineRunning == false) {
		machine, _ = mp.Triton.GetMachine(machine.Id)
		if (machine.State == "running") {
			machineRunning = true
		}
		time.Sleep(5 * time.Second)
	}
	//extract IP address
	ipAddress := machine.IPs[0]
	machineCommand := fmt.Sprintf("/usr/bin/docker-machine --debug create --driver generic --engine-storage-driver overlay2 --generic-ip-address %s --generic-ssh-key /root/.ssh/id_rsa --engine-opt \"graph=/mnt/docker\" --generic-ssh-user ubuntu %s", ipAddress, machineObj.Name)
	if (mp.Config.TritonDockerMachine.MachineInstallUrl != "") {
		machineCommand = fmt.Sprintf("MACHINE_INSTALL_URL=%s %s", mp.Config.TritonDockerMachine.MachineInstallUrl, machineCommand)
	}
	cmd := exec.Command(
		"bash",
		"-c",
		machineCommand,
	)
	//var out bytes.Buffer
	cmd.Stdout = os.Stdout //&out
	cmd.Stderr = os.Stderr //&out
	err := cmd.Run()
	if err != nil {
		machineObj.Error = err
		mp.Machines[machineName].WaitGroup.Done()
		return
	}
	machineObj.Machine = machine
	machineObj.DockerCred = docker_helpers.DockerCredentials{
		Host: fmt.Sprintf("tcp://%s:2376", ipAddress),
		CertPath: fmt.Sprintf("/root/.docker/machine/machines/%s", machineObj.Name),
		TLSVerify: false,
	}
	if err != nil {
		machineObj.Error = err
		mp.Machines[machineName].WaitGroup.Done()
		return
	}
	machineObj.State = "ready"
	machineObj.Ready = true
	mp.Machines[machineName].WaitGroup.Done()
	return
}

func (mp *machinePool) waitForMachine(machineName string) {
	mp.Machines[machineName].WaitGroup.Wait()
} 

func (mp *machinePool) LockMachineForBuild(build *common.Build) *Machine {
	mp.connectTriton()
	machineName := mp.machineNameForBuild(build)
	mp.MachinesLock.Lock()
	machineObj := mp.Machines[machineName]
	if (machineObj == nil) {
		mp.Machines[machineName] = &Machine{
			State: "setup",
			Ready: false,
			Name: machineName,
			Builds: 0,
		}
		machineObj = mp.Machines[machineName]
		machineObj.WaitGroup.Add(1)
		go func(name string) {
			mp.buildHost(name)
		}(machineName)
	}
	if (machineObj.State == "pendingRemoval") {
		//cleanup timer
		machineObj.RemoveTimer.Stop()
		machineObj.RemoveTimer = nil
		machineObj.State = "ready"
	}
	machineObj = mp.Machines[machineName]
	machineObj.Builds += 1
	mp.MachinesLock.Unlock()
	mp.waitForMachine(machineName)
	return mp.Machines[machineName]
}

func (mp *machinePool) CleanupMachine(machineName string) {
	machineObj := mp.Machines[machineName]
	mp.Triton.DeleteMachine(machineObj.Machine.Id)
	machineCommand := fmt.Sprintf("/usr/bin/docker-machine rm -y %s", machineObj.Name)
	cmd := exec.Command(
		"bash",
		"-c",
		machineCommand,
	)
	//var out bytes.Buffer
	cmd.Stdout = os.Stdout //&out
	cmd.Stderr = os.Stderr //&out
	err := cmd.Run()
	if err != nil {
		fmt.Println(err)
	}
	mp.MachinesLock.Lock()
	delete(mp.Machines, machineName)
	mp.MachinesLock.Unlock()
}

func (mp *machinePool) ReleaseMachineForBuild(machineName string) {
	machineObj := mp.Machines[machineName]
	mp.MachinesLock.Lock()
	machineObj.Builds -= 1
	machineObj.State = "pendingRemoval"
	if (machineObj.Builds == 0) {
		//kick off timer
		seconds := int64(120)
		if (mp.Config.TritonDockerMachine.MachineIdleSeconds != 0) {
			seconds = int64(mp.Config.TritonDockerMachine.MachineIdleSeconds)
		}
		machineObj.RemoveTimer = time.NewTimer(time.Duration(seconds) * time.Second)
		go func(machineName string) {
			<- machineObj.RemoveTimer.C
			mp.CleanupMachine(machineName)
		}(machineName)
	}
	mp.MachinesLock.Unlock()
}
