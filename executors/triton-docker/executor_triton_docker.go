package triton_docker

import (
	"errors"
	"fmt"
	"io"
	"os"
	"os/user"
	"io/ioutil"
	"runtime"
	"strconv"
	"strings"
	"sync"
	"time"
	"crypto/rsa"
	"crypto/rand"
	"crypto/x509"
	"encoding/pem"
	"encoding/base64"

	"golang.org/x/crypto/ssh"
	"github.com/docker/docker/pkg/homedir"
	"github.com/fsouza/go-dockerclient"
	"gitlab.com/gitlab-org/gitlab-ci-multi-runner/common"
	"gitlab.com/gitlab-org/gitlab-ci-multi-runner/executors"
	"github.com/joyent/gocommon/client"
    "github.com/joyent/gosdc/cloudapi"
    "github.com/joyent/gosign/auth"
    "github.com/satori/go.uuid"
	docker_helpers "gitlab.com/gitlab-org/gitlab-ci-multi-runner/helpers/docker"
)

type dockerOptions struct {
	Image    string   `json:"image"`
	Services []string `json:"services"`
}

type executor struct {
	executors.AbstractExecutor
	client      docker_helpers.Client
	failures    []*docker.Container
	builds      []*docker.Container
	services    []*docker.Container
	caches      []*docker.Container
	options     dockerOptions
	info        *docker.Env
	binds       []string
	volumesFrom []string
	devices     []docker.Device
	links       []string
	triton 		*cloudapi.Client
	buildKey 	*TritonBuildKey
}

func (s *executor) getServiceVariables() []string {
	return s.Build.GetAllVariables().PublicOrInternal().StringList()
}

func (s *executor) getAuthConfig(imageName string) (docker.AuthConfiguration, error) {
	homeDir := homedir.Get()
	if s.Shell().User != "" {
		u, err := user.Lookup(s.Shell().User)
		if err != nil {
			return docker.AuthConfiguration{}, err
		}
		homeDir = u.HomeDir
	}
	if homeDir == "" {
		return docker.AuthConfiguration{}, fmt.Errorf("Failed to get home directory")
	}

	indexName, _ := docker_helpers.SplitDockerImageName(imageName)

	authConfigs, err := docker_helpers.ReadDockerAuthConfigs(homeDir)
	if err != nil {
		// ignore doesn't exist errors
		if os.IsNotExist(err) {
			err = nil
		}
		return docker.AuthConfiguration{}, err
	}

	authConfig := docker_helpers.ResolveDockerAuthConfig(indexName, authConfigs)
	if authConfig != nil {
		s.Debugln("Using", authConfig.Username, "to connect to", authConfig.ServerAddress, "in order to resolve", imageName, "...")
		return *authConfig, nil
	}

	return docker.AuthConfiguration{}, fmt.Errorf("No credentials found for %v", indexName)
}

func (s *executor) pullDockerImage(imageName string) (*docker.Image, error) {
	s.Println("Pulling docker image", imageName, "...")
	authConfig, err := s.getAuthConfig(imageName)
	if err != nil {
		s.Debugln(err)
	}

	pullImageOptions := docker.PullImageOptions{
		Repository: imageName,
	}

	// Add :latest to limit the download results
	if !strings.ContainsAny(pullImageOptions.Repository, ":@") {
		pullImageOptions.Repository += ":latest"
	}

	err = s.client.PullImage(pullImageOptions, authConfig)
	if err != nil {
		if strings.Contains(err.Error(), "not found") {
			return nil, &common.BuildError{Inner: err}
		}
		return nil, err
	}

	image, err := s.client.InspectImage(imageName)
	return image, err
}

func (s *executor) getDockerImage(imageName string) (*docker.Image, error) {
	pullPolicy, err := s.Config.TritonDocker.PullPolicy.Get()
	if err != nil {
		return nil, err
	}

	s.Debugln("Looking for image", imageName, "...")
	image, err := s.client.InspectImage(imageName)

	// If never is specified then we return what inspect did return
	if pullPolicy == common.DockerPullPolicyNever {
		return image, err
	}

	if err == nil {
		// Don't pull image that is passed by ID
		if image.ID == imageName {
			return image, nil
		}

		// If not-present is specified
		if pullPolicy == common.DockerPullPolicyIfNotPresent {
			return image, err
		}
	}

	newImage, err := s.pullDockerImage(imageName)
	if err != nil {
		if image != nil {
			s.Warningln("Cannot pull the latest version of image", imageName, ":", err)
			s.Warningln("Locally found image will be used instead.")
			return image, nil
		}
		return nil, err
	}
	return newImage, nil
}

func (s *executor) getArchitecture() string {
	architecture := s.info.Get("Architecture")
	switch architecture {
	case "armv7l", "aarch64":
		architecture = "arm"
	case "amd64":
		architecture = "x86_64"
	}

	if architecture != "" {
		return architecture
	}

	switch runtime.GOARCH {
	case "amd64":
		return "x86_64"
	default:
		return runtime.GOARCH
	}
}

func (s *executor) getLabels(containerType string, otherLabels ...string) map[string]string {
	labels := make(map[string]string)
	labels[dockerLabelPrefix+".build.id"] = strconv.Itoa(s.Build.ID)
	labels[dockerLabelPrefix+".build.sha"] = s.Build.Sha
	labels[dockerLabelPrefix+".build.before_sha"] = s.Build.BeforeSha
	labels[dockerLabelPrefix+".build.ref_name"] = s.Build.RefName
	labels[dockerLabelPrefix+".project.id"] = strconv.Itoa(s.Build.ProjectID)
	labels[dockerLabelPrefix+".runner.id"] = s.Build.Runner.ShortDescription()
	labels[dockerLabelPrefix+".runner.local_id"] = strconv.Itoa(s.Build.RunnerID)
	labels[dockerLabelPrefix+".type"] = containerType
	if (s.Config.TritonDocker.DefaultPackage != "") {
		labels["com.joyent.package"] = s.Config.TritonDocker.DefaultPackage
	}
	for _, label := range otherLabels {
		keyValue := strings.SplitN(label, "=", 2)
		if len(keyValue) == 2 {
			labels[dockerLabelPrefix+"."+keyValue[0]] = keyValue[1]
		}
	}
	return labels
}

func (s *executor) createContainer(containerType, imageName string, cmd []string) (container *docker.Container, err error) {
	s.Debugln("Fetching Image Name ", imageName)
	image, err := s.getDockerImage(imageName)
	if err != nil {
		return nil, err
	}
	hostname := s.Config.TritonDocker.Hostname
	if hostname == "" {
		hostname = s.Build.ProjectUniqueName()
	}
	containerName := fmt.Sprintf("%s-%s", s.Build.ProjectUniqueName(), containerType)
	tritonVariables := []string{}
	encodedPrivateKey := base64.StdEncoding.EncodeToString([]byte(s.buildKey.PrivateKey))
	tritonVariables = append(tritonVariables, fmt.Sprintf("T_KEY=%s", encodedPrivateKey))
	if (s.Config.TritonDocker.TritonHost != "") {
		tritonVariables = append(tritonVariables, fmt.Sprintf("T_URL=%s", s.Config.TritonDocker.TritonHost))
	} 
	if (s.Config.TritonDocker.MantaHost != "") {
		tritonVariables = append(tritonVariables, fmt.Sprintf("M_URL=%s", s.Config.TritonDocker.MantaHost))
	} 
	if (s.Config.TritonDocker.TritonUser != "") {
		tritonVariables = append(tritonVariables, fmt.Sprintf("T_USER=%s", s.Config.TritonDocker.TritonUser))
	}
	options := docker.CreateContainerOptions{
		Name: containerName,
		Config: &docker.Config{
			Image:        image.ID,
			Hostname:     containerName,
			Labels:       s.getLabels(containerType),
			Tty:          false,
			AttachStdin:  true,
			AttachStdout: true,
			AttachStderr: true,
			OpenStdin:    true,
			StdinOnce:    true,
			Env:          append(s.Build.GetAllVariables().StringList(), s.BuildShell.Environment...),
		},
		HostConfig: &docker.HostConfig{
			RestartPolicy: docker.NeverRestart(),
			LogConfig: docker.LogConfig{
				Type: "json-file",
			},
		},
	}
	options.Config.Env = append(options.Config.Env, tritonVariables...)
	s.removeContainer(containerName)
	s.Debugln("Creating container", options.Name, "...")
	container, err = s.client.CreateContainer(options)
	if err != nil {
		s.Debugln("Error creating container", err)
		if container != nil {
			s.failures = append(s.failures, container)
		}
		return nil, err
	}
	s.builds = append(s.builds, container)
	return
}

func (s *executor) killContainer(container *docker.Container, waitCh chan error) (err error) {
	for {
		s.Debugln("Killing container", container.ID, "...")
		s.client.KillContainer(docker.KillContainerOptions{
			ID: container.ID,
		})

		// Wait for signal that container were killed
		// or retry after some time
		select {
		case err = <-waitCh:
			return

		case <-time.After(time.Second):
		}
	}
}

func (s *executor) watchContainer(container *docker.Container, input io.Reader, abort chan interface{}) (err error) {
	s.Debugln("Starting container", container.ID, "...")
	err = s.client.StartContainer(container.ID, nil)
	if err != nil {
		return
	}
	var parsedString string
	if b, err := ioutil.ReadAll(input); err == nil {
	    parsedString = string(b)
	}
	if err != nil {
		s.Debugln("Error Reading Input/Command String", err);
		return
	}
	cmdArgs := []string{"bash_shell", "-c", parsedString}
	execOpts := docker.CreateExecOptions{
		AttachStdin: true,
		AttachStdout: true,
	    AttachStderr: true,
	    Tty:          false,
	    Cmd:          cmdArgs,
	    Container:    container.ID,
	}
	exec, execErr := s.client.CreateExec(execOpts)
	if execErr != nil {
		s.Debugln("Error Creating Exec", execErr);
		return
	}
	s.Debugln("Exec created with ID", exec.ID)
	inputStream, inputWriterStream := io.Pipe()
	startExecOpts := docker.StartExecOptions{
		InputStream: inputStream,
	    OutputStream: s.BuildTrace,
	    ErrorStream: s.BuildTrace,
	    Detach: false,
	    Tty: false,
	    RawTerminal: false,
	}
	waitCh := make(chan error, 1)
	go func() {
		s.Debugln("Execing in container", container.ID, "...")
		keyTicker := time.NewTicker(time.Minute * 1)
		go func() {
	        for t := range keyTicker.C {
	        	s.Debugln("Sending exec keep alive at", t, "...")
	            io.WriteString(inputWriterStream, "\n")
	        }
	    }()
		startExecError := s.client.StartExec(exec.ID, startExecOpts)
		if startExecError != nil {
			waitCh <- startExecError
			return
		}
		keyTicker.Stop()
		s.Debugln("Fetching exit code for exec", container.ID, "...")
		execInspect, inspectErr := s.client.InspectExec(exec.ID)
		s.Debugln("EXEC Inspect", fmt.Sprintf("%+v", execInspect), "...")
		s.Debugln("EXEC Error", fmt.Sprintf("%+v", inspectErr), "...")
		if inspectErr == nil {
			if execInspect.ExitCode != 0 {
				inspectErr = &common.BuildError{
					Inner: fmt.Errorf("exit code %d", execInspect.ExitCode),
				}
			}
		}
		waitCh <- inspectErr
	}()
	select {
		case <-abort:
			s.killContainer(container, waitCh)
			err = errors.New("Aborted")

		case err = <-waitCh:
			s.Debugln("Container", container.ID, "finished with", err)
	}
	return
}

func (s *executor) removeContainer(id string) error {
	removeContainerOptions := docker.RemoveContainerOptions{
		ID:            id,
		RemoveVolumes: true,
		Force:         true,
	}
	err := s.client.RemoveContainer(removeContainerOptions)
	s.Debugln("Removed container", id, "with", err)
	return err
}

func (s *executor) getImageName() (string, error) {
	if s.options.Image != "" {
		image := s.Build.GetAllVariables().ExpandValue(s.options.Image)
		return image, nil
	}

	if s.Config.TritonDocker.Image == "" {
		return "", errors.New("No Docker image specified to run the build in")
	}

	return s.Config.TritonDocker.Image, nil
}

func (s *executor) connectDocker() (err error) {
	client, err := docker_helpers.New(s.Config.TritonDocker.DockerCredentials, DockerAPIVersion)
	if err != nil {
		return err
	}
	s.client = client

	s.info, err = client.Info()
	if err != nil {
		return err
	}

	return
}

func (s *executor) connectTriton() (err error) {
	keyPath := fmt.Sprintf("%s/key.pem", s.Config.TritonDocker.CertPath)
	fmt.Println("Key Path", keyPath)
	keyData, err := ioutil.ReadFile(keyPath)
	if err != nil {
		return err
	}
	userAuth, err := auth.NewAuth(s.Config.TritonDocker.TritonUser, string(keyData), "rsa-sha256")
	if err != nil {
		return err
	}
	fmt.Println(fmt.Sprintf("%+v", userAuth))
	creds := &auth.Credentials{
        UserAuthentication: userAuth,
        SdcKeyId:           s.Config.TritonDocker.TritonKeyId,
        SdcEndpoint:        auth.Endpoint{URL: s.Config.TritonDocker.TritonHost},
    }
    fmt.Println(fmt.Sprintf("%+v", creds))
    s.triton = cloudapi.New(client.NewClient(
        creds.SdcEndpoint.URL,
        cloudapi.DefaultAPIVersion,
        creds,
        nil,
    ))
    _, err = s.triton.ListKeys()
    if err != nil {
		return err
	}
    return
}

type TritonBuildKey struct {
	Name string
	Fingerprint string
	Key string
	PrivateKey string
}

func (s *executor) scrubExpiredKeys() (err error) {
	keys, err := s.triton.ListKeys()

	for _, key := range keys {
		keyNameParts := strings.Split(key.Name, "-")
		if (len(keyNameParts) != 4) {
			continue
		}
		if ((keyNameParts[0] != "Gitlab") || (keyNameParts[0] != "Build")) {
			continue
		}
		expiryString := keyNameParts[3]
		intValue, err := strconv.Atoi(expiryString)
		if (err != nil) {
			continue
		}
		int64Value := int64(intValue)
		expiryTime := time.Unix(int64Value, 0)
		currentTime := time.Now().UTC()
		if (!currentTime.Before(expiryTime)) {
			s.Debugln("Removing expired key", key.Name, "...")
			keyErr := s.triton.DeleteKey(key.Name)
			if (keyErr != nil) {
				s.Debugln("Error removing expired key", key.Name, keyErr, "...")
			}
		}
	}
	return nil
}

func (s *executor) registerBuildKey() (err error) {
	s.scrubExpiredKeys()
	privateKey, err := rsa.GenerateKey(rand.Reader, 1024)
    if err != nil {
        return err
    }
    privateKeyPEMBlock := &pem.Block{
    	Type: "RSA PRIVATE KEY", 
    	Bytes: x509.MarshalPKCS1PrivateKey(privateKey),
    }
    privateKeyPEM := pem.EncodeToMemory(privateKeyPEMBlock)
    pub, err := ssh.NewPublicKey(&privateKey.PublicKey)
    if err != nil {
        return err
    }
    publicKeySSH := ssh.MarshalAuthorizedKey(pub)
	currentTime := time.Now().UTC()
	validityWindowMinutes := 60
	if (s.Config.TritonDocker.BuildKeyValidityWindow != 0) {
		validityWindowMinutes = s.Config.TritonDocker.BuildKeyValidityWindow
	}
	expiryTime := currentTime.Add(time.Duration(validityWindowMinutes)*time.Minute)
    tritonKey, err := s.triton.CreateKey(cloudapi.CreateKeyOpts{
    	Name: fmt.Sprintf("Gitlab-Build-%s-%d", uuid.NewV4(), expiryTime.Unix()),
    	Key: string(publicKeySSH),
	})
    if err != nil {
        return err
    }

    returnKey := &TritonBuildKey{
    	Name: tritonKey.Name,
    	Fingerprint: tritonKey.Fingerprint,
    	Key: tritonKey.Key,
    	PrivateKey: string(privateKeyPEM),
	}
	s.buildKey = returnKey;
    return nil
}

func (s *executor) Prepare(globalConfig *common.Config, config *common.RunnerConfig, build *common.Build) error {

	err := s.AbstractExecutor.Prepare(globalConfig, config, build)
	if err != nil {
		return err
	}

	if s.BuildShell.PassFile {
		return errors.New("Docker doesn't support shells that require script file")
	}

	if config.TritonDocker == nil {
		return errors.New("Missing triton docker configuration")
	}

	err = build.Options.Decode(&s.options)
	if err != nil {
		return err
	}

	imageName, err := s.getImageName()
	if err != nil {
		return err
	}

	s.Println("Using Docker executor with image", imageName, "...")

	err = s.connectDocker()
	if err != nil {
		return err
	}
	err = s.connectTriton()
	if err != nil {
		return err
	}
	err = s.registerBuildKey()
	if err != nil {
		return err
	}
	return nil
}

func (s *executor) Cleanup() {
	var wg sync.WaitGroup

	remove := func(id string) {
		wg.Add(1)
		go func() {
			s.removeContainer(id)
			wg.Done()
		}()
	}

	for _, failure := range s.failures {
		remove(failure.ID)
	}

	for _, build := range s.builds {
		remove(build.ID)
	}

	if (s.buildKey != nil) {
		keyErr := s.triton.DeleteKey(s.buildKey.Name)

		if (keyErr != nil) {
			s.Debugln("Error removing build key from Triton", keyErr, "...")
		}
	}

	wg.Wait()

	if s.client != nil {
		docker_helpers.Close(s.client)
	}

	s.AbstractExecutor.Cleanup()
}

