package triton_docker

const DockerAPIVersion = "1.23"
const dockerLabelPrefix = "com.gitlab.gitlab-runner"

const prebuiltImageName = "gitlab-runner-prebuilt"
const prebuiltImageExtension = ".tar.xz"
